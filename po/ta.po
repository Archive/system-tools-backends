# translation of ta.po to Tamil
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Jayaradha N <jaya@pune.redhat.com>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: ta\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-09-03 12:22+0200\n"
"PO-Revision-Date: 2004-09-04 16:41+0530\n"
"Last-Translator: Jayaradha N <jaya@pune.redhat.com>\n"
"Language-Team: Tamil <zhakanini@yahoogroups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=(n != 1);\n\n"

#. please, keep this list sorted
#. service => [long_name, description]
#: service-list.pl.in:36
msgid "ACPI event handler"
msgstr "ACPI நிகழ்வு கையாள்பவர்"

#: service-list.pl.in:36
msgid ""
"Notifies user-space programs of ACPI events: executes arbitrary commands in "
"response to ACPI events such as power buttons."
msgstr "ACPI நிகவின் user-space நிரலுக்கு செய்தி அனுப்பும். ACPI தொடர்புடைய செய்திகளையும் கட்டளைகளையும் இயக்கும்"

#: service-list.pl.in:37
msgid "Advanced Linux Sound Architecture"
msgstr "மேம்பட்ட லினக்ஸ் ஒலி கட்டமைப்பு"

#: service-list.pl.in:38 service-list.pl.in:70
msgid "web server"
msgstr "இணைய சேவகன்"

#: service-list.pl.in:38 service-list.pl.in:70
msgid "Apache is a World Wide Web server.  It is used to serve HTML files and CGI."
msgstr "அப்பேச்சி உலகலாவிய வலை சேவகம். இது HTML மற்றும் CGI கோப்புகளுக்கான சேவையை செய்யும்"

#: service-list.pl.in:39
msgid "Run missed scheduled jobs at startup"
msgstr "துவக்கத்தில் விடுபட்ட ஆனால் அட்டவணையில் உள்ள செயல்களை செய்யும்"

#: service-list.pl.in:39
msgid "Run cron jobs that were left out due to downtime"
msgstr "நிறுத்தியதால் விடுபட்டுப்போன cron வேலைகளை செய்யும்"

#: service-list.pl.in:40
msgid "Automatic power management daemon"
msgstr "தானாக மின்சாரத்தை கையாளும் மாயாவி"

#: service-list.pl.in:40
msgid ""
"apmd is used for monitoring battery status and logging it via syslog(8). It "
"can also be used for shutting down the machine when the battery is low."
msgstr "apmd syslog(8) வழியாக மின்கல நிலையை கண்காணிக்க பயன்படும் சாதனம். மின்கலனில் மின்சாரம் குறைவாக இருக்கும் போது கணினியை நிறுத்த இது பயன்படும்."

#: service-list.pl.in:41
msgid "Runs a command at a given time"
msgstr "குறித்த நேரத்தில் கட்டளையை இயக்கும்"

#: service-list.pl.in:41
msgid ""
"Runs commands scheduled by the at command at the time specified when at was "
"run, and runs batch commands when the load average is low enough."
msgstr "இயங்க வேண்டிய இடத்தில் குறித்த நேரத்தில் கட்டளை இயங்கும், குழு கட்டளைகள் ஏற்றப்படும் போது குறைவாக இருக்கும்"

#: service-list.pl.in:42
msgid "Audio mixer"
msgstr "ஒலி கலக்கி"

#: service-list.pl.in:42
msgid "This program adjusts the settings of an audio mixing device"
msgstr "இந்த கட்டளை ஒலிகலக்கி சாதனத்திற்கான நிரலை சரியாக அமைக்க உதவும்"

#: service-list.pl.in:43
msgid "BinFormat Support"
msgstr "BinFormat ஆதரவு"

#: service-list.pl.in:43
msgid "Support for running non ELF binaries on Linux"
msgstr "ELF பைனரிகளை லினக்ஸில் இயக்க ஆதரவு"

#: service-list.pl.in:44
msgid "Boot messages recorder"
msgstr "துவக்க செய்தி பதிப்பாளர்"

#: service-list.pl.in:44
msgid ""
"Runs in the background and copies all strings sent to the /dev/console "
"device to a logfile"
msgstr "இது பின்னனியில் இயங்கி எல்லா சரங்களையும் /dev/console சாதன பதிவேட்டில் நகலெடுக்கும்"

#: service-list.pl.in:45
msgid ""
"An xinetd internal service which generate characters. The xinetd internal "
"service which continuously generates characters until the connection is "
"dropped.  This is the tcp version. You must enable xinetd to use this "
"service."
msgstr "xinetd எழுத்துக்களை உருவாக்கும். xinetd உள் சேவை இணைப்பு அருபடும் வரை எழுத்துக்களை உருவாக்கும். இந்த tcp பதிப்பில்.  xinetd சேவையை செயல்படுத்த வேண்டும்"

#: service-list.pl.in:46
msgid ""
"An xinetd internal service which generate characters. The xinetd internal "
"service which continuously generates characters until the connection is "
"dropped.  This is the udp version. You must enable xinetd to use this "
"service."
msgstr "xinetd எழுத்துக்களை உருவாக்கும். xinetd உள் சேவை இணைப்பு அருபடும் வரை எழுத்துக்களை உருவாக்கும். இந்த udp பதிப்பில். xinetd சேவையை செயல்படுத்த வேண்டும்"

#: service-list.pl.in:47
msgid "CPU frequency monitor"
msgstr "CPU அதிர்வெண்கண்கானிப்பாளர்"

#: service-list.pl.in:47
msgid ""
"cpufreqd is used to monitor the status of the battery and adjust the "
"frequency of the CPU accordingly in order to preserve battery power while "
"providing optimal performance"
msgstr "cpufreqd மின்கலத்தின் நிலையை கண்காணித்து அதன் அதிர்வெண்ணை CPU விற்கு ஏற்ப மின்சாரத்தை தந்து கணினி சரியாக வேலைசெய்ய னிக்க பயனrmance"

#: service-list.pl.in:48 service-list.pl.in:130
msgid "daemon to execute scheduled commands"
msgstr "அட்டவணைப்படுத்தப்பட்ட கட்டளைகளை இயக்கும் மாயாவி"

#: service-list.pl.in:49
msgid "Run scheduled jobs"
msgstr "இந்த அட்டவணைப்படுத்தப்பட்ட வேலைகளை செய்"

#: service-list.pl.in:49
msgid ""
"cron is a standard UNIX program that runs user-specified programs at "
"periodic scheduled times. vixie cron adds a number of features to the basic "
"UNIX cron, including better security and more powerful configuration options."
msgstr "cron யூனிக்ஸ் நிரல் பயனீட்டாளர் குறித்த நிரலை சரியான நேரத்தில் இயக்கும். vixie cron அடிப்படை யூனிக்ஸ் கட்டளைகளை UNIX cron இயக்கி, பாதுகாப்பு மற்றும் திறனுள்ள தேர்வை தரும்"

#: service-list.pl.in:50 service-list.pl.in:51
msgid "Common Unix Printing System"
msgstr "பொதுவான யூனிக்ஸ் அச்சடிக்கும் முறைமை"

#: service-list.pl.in:50 service-list.pl.in:51
msgid "Printing system based upon the Internet Printing Protocol, version  1.1"
msgstr "இணைய அச்சடிக்கும் விதிமுறையை தழுவிய அச்சடிக்கும் அமைப்பு, பதிப்பு 1.1"

#: service-list.pl.in:52
msgid ""
"An internal xinetd service which gets the current system time then prints it "
"out in a format like this: 'Wed Nov 13 22:30:27 EST 2002'.  This is the tcp "
"version. You must enable xinetd to use this service."
msgstr ""
"xinetd சேவை தற்போதைய கணினி நேரத்தை பெற்று அதை கீழ்கண்ட வடிவத்தில் அச்சடிக்கும்: 'Wed Nov 13 22:30:27 EST 2002'.  இது tcp "
"பதிப்பு. இதற்கு xinetd சேவையை பயன்படுத்தவேண்டும்."

#: service-list.pl.in:53
msgid ""
"An internal xinetd service which gets the current system time then prints it "
"out in a format like this: 'Wed Nov 13 22:30:27 EST 2002'.  This is the udp "
"version. You must enable xinetd to use this service."
msgstr ""
"xinetd சேவை தற்போதைய கணினி நேரத்தை பெற்று அதை கீழ்கண்ட வடிவத்தில் அச்சடிக்கும்: 'Wed Nov 13 22:30:27 EST 2002'.  இது ucp "
"பதிப்பு. இதற்கு xinetd சேவையை பயன்படுத்தவேண்டும்."

#: service-list.pl.in:54 service-list.pl.in:55
msgid "Dynamic DNS Client"
msgstr "செயல் DNS புரவலன்"

#: service-list.pl.in:54
msgid "A Dynamic DNS Name service from www.dyndns.org"
msgstr "www.dyndns.org விலிருந்து செயல்பாட்டு DNS பெயர் சேவை "

#: service-list.pl.in:55
msgid "A Dynamic DNS Name service from www.dhis.org"
msgstr "www.dhis.org விலிருந்து செயல்பாட்டு DNS பெயர் சேவை "

#: service-list.pl.in:56
msgid "Dict Dictionary Server"
msgstr "Dict அகராதி சேவை"

#: service-list.pl.in:57
msgid ""
"An xinetd internal service which echo's characters back to clients.  This is "
"the tcp version. You must enable xinetd to use this service."
msgstr ""
"xinetd சேவை echo's எழுத்துக்களை புரவலனில் பயன்படுத்துகிறது. இது "
"tcp பதிப்பு. இதற்கு xinetd சேவையை பயன்படுத்த வேண்டும்."

#: service-list.pl.in:58
msgid ""
"An xinetd internal service which echo's characters back to clients.  This is "
"the udp version. You must enable xinetd to use this service."
msgstr ""
"xinetd சேவை echo's எழுத்துக்களை புரவலனில் பயன்படுத்துகிறது. இது "
"ucp பதிப்பு. இதற்கு xinetd சேவையை பயன்படுத்த வேண்டும்."

#: service-list.pl.in:59
msgid "Sound mixing server"
msgstr "ஒலி கலக்கி சேவகன்"

#: service-list.pl.in:59
msgid "The Enlightened Sound Daemon"
msgstr "மேம்பட்ட ஒலி மாயாவி"

#: service-list.pl.in:60
msgid "Exim SMTP Server"
msgstr "Exim SMTP சேவகன்"

#: service-list.pl.in:61
msgid "file alteration monitor"
msgstr "கோப்பு திருத்த கண்கானிப்பு"

#: service-list.pl.in:62
msgid "Firstboot is a druid style program that runs on the first time"
msgstr "Firstboot ட்டூரல் பாணி நிரல் முதல் முறை இயங்கும்"

#: service-list.pl.in:63
msgid "Text-to-speech system"
msgstr "உரை யிலிருந்து பேச்சு கணினி"

#: service-list.pl.in:63
msgid "Festival is a general purpose text-to-speech system"
msgstr "ஃபெஸ்டிவெல் என்பது பொதுவான உரை யிலிருந்து பேச்சு கணினி"

#: service-list.pl.in:64
msgid "Mail Fetcher"
msgstr "மின்னஞ்சல் பெறுதல்"

#: service-list.pl.in:64
msgid "A Mail Fetcher from external Servers"
msgstr "வெளி சேவகனிலிருந்து மின்னஞ்சல் பெறும் வசதி"

#: service-list.pl.in:65
msgid "IPv6 Tunnel Broker"
msgstr "IPv6 குகை தரகர்"

#: service-list.pl.in:65
msgid "A IPv6 Dynamic Tunnel Broker"
msgstr "IPv6 இயக்க சுரங்க தரகர்"

#: service-list.pl.in:66
msgid "GNOME Display Manager"
msgstr "க்னோம் காட்சி மேலாளர்"

#: service-list.pl.in:66
msgid "gdm provides the GNOME equivalent of a \"login:\" prompt."
msgstr "gdm க்னோம் உள்நுழை வசதி \"login:\""

#: service-list.pl.in:67
msgid "console mouse support"
msgstr "கன்சோம் சுட்டி ஆதரவு"

#: service-list.pl.in:67
msgid ""
"GPM adds mouse support to text-based Linux applications such the Midnight "
"Commander. It also allows mouse-based console cut-and-paste operations, and "
"includes support for pop-up menus on the console."
msgstr "GPM உரை-சார்ந்த லினக்ஸ் பயன்பாட்டி சுட்டியை பயன்படுத்த வழி செய்யும். இது முனையத்தில் சுட்டியை பயன்படுத்து வெட்டு, ஒட்டு வேலைகளை செய்யவும் தோன்றும் சாளரங்களை செயல்படுத்தும்"

#: service-list.pl.in:68
msgid "Set hard disk parameters"
msgstr "வன்தகடு அளவுருக்களை அமை"

#: service-list.pl.in:68
msgid ""
"Provides a interface to various hard disk ioctls supported by the stock "
"Linux ATA/IDE device driver subsystem."
msgstr ""
"பல வன்தகடுகளுக்கான ioctls ஆதரவு தரும்"
"Linux ATA/IDE சாதன இயக்கு வழி ஏற்படுத்தும்"

#: service-list.pl.in:69
msgid "Linux hotplugging support"
msgstr "லினக்ஸ் உடனடி சொருகல் ஆதரவு"

#: service-list.pl.in:69
msgid ""
"hotplug is a program which is used by the kernel to notify user mode "
"software when some significant (usually hardware-related) events take place. "
"An example is when a USB or Cardbus device has just been plugged in."
msgstr ""
"குறிப்பிட்ட சில நிகழ்வுகளின் போது hotplug ஐ பயன்படுத்தி கர்னல் பயனீட்டாளருக்கு செய்தி அளிக்கும் (பொதுவாக வன்பொருள் சார்ந்த)."
"உதாரணம் USB அல்லது Cardbus சாதனம் சொருகப்படல்."

#: service-list.pl.in:71
msgid "internet superserver"
msgstr "இணைய மேலாண்மை சேவகன்"

#: service-list.pl.in:71
msgid ""
"inetd allows running one daemon to invoke several others, reducing load on "
"the system."
msgstr "inetd நிரலை மாயாவி மூலம் துவக்கி ஏற்ற பலுவை குறைக்கும்"

#: service-list.pl.in:72
msgid "administration tool for IPv4 packet filtering and NAT"
msgstr "IPv4 பொதிவடிகட்டி மற்றும் NAT மேலாண்மை கருவி"

#: service-list.pl.in:72
msgid ""
"Iptables is used to set up, maintain, and inspect the tables of IP packet "
"filter rules in the Linux kernel."
msgstr "Iptables  IP கட்டுகளின் விதிகளை அமைத்து அவைகளை கண்காணிக்க லினக்ஸ் கர்னல் பயன்படுத்தும்"

#: service-list.pl.in:73
msgid "IrDA stack for Linux"
msgstr "லினக்ஸ் ஸ்டாக் IrDA"

#: service-list.pl.in:74
msgid "The Internet Key Exchange protocol"
msgstr "கர்னர் விசை மாற்று விதி"

#: service-list.pl.in:74
msgid "The Internet Key Exchange protocol openbsd implementation"
msgstr "இணைய விசை மாற்ற விதி openbsd செயல்பாடு"

#: service-list.pl.in:75
msgid "start and stop ISDN services"
msgstr "ISDN சேவையை துவக்கு நிறுத்து"

#: service-list.pl.in:76
msgid "Check and fix joystick device nodes"
msgstr "joystick சாதனத்தை சோதித்து பொருத்து"

#: service-list.pl.in:76
msgid ""
"Runs joystick-device-check.  joystick-device-check is a program which "
"ensures that the /dev/js* device nodes are correct for the kernel version in "
"use."
msgstr ""
"joystick-device-check.  joystick-device-check நிரல்"
"/dev/js* கர்னல் பதிப்பில் சரியாக பொருந்துகிறதா என சோதிக்கும்"

#: service-list.pl.in:77
msgid ""
"This package loads the selected keyboard map as set in /etc/sysconfig/"
"keyboard.  This can be selected using the kbdconfig utility.  You should "
"leave this enabled for most machines."
msgstr "இந்த கட்டு தேர்வு செய்த விசையை /etc/sysconfig/ விசைப்பலகைப்குள் ஏற்றும். இதை kbdconfig பயன்பாட்டை பயன்படுத்தி தேர்வு செய்ய முடியும். இதை செயல்பாட்டில் வைக்க வேண்டும்"

#: service-list.pl.in:78
msgid "Kernel Log Daemon"
msgstr "கர்னல் பதிவு மாயாவி"

#: service-list.pl.in:78
msgid "klogd is a system daemon which intercepts and logs Linux kernel messages."
msgstr "klogd லினக்ஸ் கர்னல் செய்திகளை பதிவு செய்யும் கணினி மாயாவி"

#: service-list.pl.in:79
msgid "This runs the hardware probe, and optionally configures changed hardware."
msgstr "இது வன்பொருள் தேடலை இயக்குவதோடு அவைகளை அமைக்கவும் பயன்படும்"

#: service-list.pl.in:80 service-list.pl.in:81
msgid ""
"LIRC is a package that allows you to decode and send infra-red signals of "
"many commonly used remote controls"
msgstr "LIRC தொலை கட்டுப்படுத்திகளுக்கான அக சிவப்பு கட்டுப்பாடுகளை குறிமுறை மாற்றை செலுத்த அனுமதிக்கும்"

#: service-list.pl.in:82
msgid "Starts misc programs that should be started"
msgstr "துவக்க வேண்டிய வேறு பயன்பாடுகளை துவக்கும்"

#: service-list.pl.in:83
msgid ""
"lpd is the print daemon required for lpr to work properly. It is basically a "
"server that arbitrates print jobs to printer(s)."
msgstr "lpd lpr சரியாக வேலை செய்ய பயன்படும் அச்சியந்திர மாயாவி. இது அச்சடித்தலை கண்காணிக்கும் சேவகன்"

#: service-list.pl.in:84
msgid "Create devices"
msgstr "சாதனத்தை உருவாக்கு"

#: service-list.pl.in:84
msgid ""
"Script that will create the devices in /dev used to interface with drivers "
"in the kernel"
msgstr "/dev கர்னலோடு தொடர்புகொண்டு சாதனங்களை உருவாக்க பயன்படும் சிறுநிரல்"

#: service-list.pl.in:85
msgid "Syslog Replacement"
msgstr "Syslog மாற்றம்"

#: service-list.pl.in:85
msgid "Modern logging daemon"
msgstr "புதிய உள்நுழை மாயாவி"

#: service-list.pl.in:86
msgid "Load kernel modules"
msgstr "கர்னல் பகுதிகளை ஏற்று"

#: service-list.pl.in:87
msgid "tools for managing Linux kernel modules"
msgstr "லினக்ஸ் கர்னலை மேலாண்மை செய்யும் கருவிகள்"

#: service-list.pl.in:88 service-list.pl.in:105
msgid "database server"
msgstr "தரவுத்தள சேவகன்"

#: service-list.pl.in:88
msgid "MySQL, a SQL-based relational database daemon"
msgstr "MySQL, a SQL-based relational தரவுத்தள மாயாவி"

#: service-list.pl.in:89
msgid "dns server"
msgstr "dns சேவகன்"

#: service-list.pl.in:89
msgid ""
"named (BIND) is a Domain Name Server (DNS) that is used to resolve host "
"names to IP addresses."
msgstr "பெயருள்ள (BIND) டொமைன் பெயர் சேவகன் (DNS) புரவலன் பெயரை IP முகவரியிலிருந்து கண்டுபிடிக்க பயன்படும்."

#: service-list.pl.in:90
msgid ""
"Mounts and unmounts all Network File System (NFS), SMB (Lan Manager/"
"Windows), and NCP (NetWare) mount points."
msgstr "எல்லா வலைபின்னல் கோப்பு அமைப்புகளையும் ஏற்ற இறக்க பயன்படும் (NFS), SMB (Lan Manager/Windows), மற்றும் NCP (NetWare) ஏற்றப்புள்ளிகள்"

#: service-list.pl.in:91
msgid ""
"Activates/Deactivates all network interfaces configured to start at boot "
"time."
msgstr "எல்லா வலைப்பின்னல் இடைமுக அமைப்புகளையும் துவக்கத்தின் போது செயல்படுத்து/செயல்படுத்தாதேபடுத்து/செயல்படுத்த"

#: service-list.pl.in:92
msgid ""
"NFS is a popular protocol for file sharing across TCP/IP networks. This "
"service provides NFS server functionality, which is configured via the /etc/"
"exports file."
msgstr "NFS TCP/IP வலைப்பின்னல் வழியாக கோப்புகளை பகிர்ந்துகொள்ள பயன்படும் விதிமுறை, இது /etc/ கோப்பு மேலேற்றம் வழியாக அமைக்கப்பட்டுள்ளது."

#: service-list.pl.in:93
msgid ""
"NFS is a popular protocol for file sharing across TCP/IP networks. This "
"service provides NFS file locking functionality."
msgstr "NFS TCP/IP வலைப்பின்னல் வழியாக கோப்புகளை பகிர்ந்துகொள்ள பயன்படும் விதிமுறை, இந்த வசதி கோப்புகளை பூட்டுவதற்காக அமைக்கப்பட்டுள்ளது,"

#: service-list.pl.in:94
msgid "Name service cache daemon"
msgstr "பெயர் சேவகன் தற்காலிக மாயாவி"

#: service-list.pl.in:94
msgid "Daemon that provides a cache for the most common name service requests."
msgstr "பொதுவான பெயர் சேவகன் கோரிக்கைக்கு தற்காலிக தகவல்களை தரும் மாயாவி"

#: service-list.pl.in:95
msgid "Network Time Protocol daemon"
msgstr "வலைப்பின்னல் நேர விதிமுறை மாயாவி"

#: service-list.pl.in:95
msgid "ntpd is the NTPv4 daemon."
msgstr "ntpd என்பது NTPv4 மாயாவி"

#: service-list.pl.in:96 service-list.pl.in:97
msgid "Update the system time over the network"
msgstr "வலைப்பின்னல் வழியாக கோப்பு அமைப்புகளை புதுப்பி"

#: service-list.pl.in:96 service-list.pl.in:97
msgid ""
"Checks the time from a Network Time Protocol (ntp) server and adjusts the "
"system time accordingly."
msgstr "வலைப்பின்னலிருந்து வலைப்பின்னல் நேர விதிமுறை(ntp) சேவகனை பார்த்து நேரத்தை மாற்றவும்."

#: service-list.pl.in:98
msgid "Network Time Protocol (NTP) server"
msgstr "(NTP) வலைப்பின்னல் நேர விதிமுறை சேவகன்"

#: service-list.pl.in:98
msgid ""
"Daemon which sets and maintains the system time-of-day in synchronism with "
"Internet standard time servers"
msgstr "கணினி நேரத்தை வலைப்பின்னல் நேரத்தோடு ஒத்திசைக்கும் மாயாவி"

#: service-list.pl.in:99
msgid "HTTP caching proxy"
msgstr "HTTP கேச்சிங் ப்ராக்ஸி"

#: service-list.pl.in:100
msgid "PCMCIA Services"
msgstr "PCMCIA சேவைகள்"

#: service-list.pl.in:101
msgid "Caching-Only DNS Server"
msgstr "தற்காலிக சேமிப்பு DNS சேவகன்"

#: service-list.pl.in:102
msgid "IPsec tunnel implementation"
msgstr "IPsec சுரங்க செயல்முறை"

#: service-list.pl.in:103
msgid "DARPA port to RPC program number mapper"
msgstr "DARPA RPC நிரல் எண் அமைப்பு"

#: service-list.pl.in:103
msgid ""
"The portmapper manages RPC connections, which are used by protocols such as "
"NFS and NIS. The portmap server must be running on machines which act as "
"servers for protocols which make use of the RPC mechanism."
msgstr ""
"போர்ட் மேப்பர் RPC இணைப்புகளை மேலாண்மை செய்வதோடு, "
"NFS மற்றும் IS. விதிமுறைகளை பயன்படுத்தும் TPC mதொழில்நுட்பத்தை பயன்படுத்தும் போது போர்ட் மேப் சேவகனில் இயக்கப்பட வேண்டும்"

#: service-list.pl.in:104
msgid "Mail Transport Agent"
msgstr "மின்னஞ்சல் அனுப்பும் தரகர்"

#: service-list.pl.in:106
msgid "Point to Point Protocol daemon"
msgstr "Point to Point விதிமுறை மாயாவி"

#: service-list.pl.in:106
msgid ""
"The  Point-to-Point Protocol (PPP) provides a method for transmitting "
"datagrams over serial point-to-point links."
msgstr "Point-to-Point Protocol (PPP) டேட்டா க்ராம்களை வரசையாக அனுப்ப உதவும்"

#: service-list.pl.in:107
msgid "Privacy enhancing HTTP Proxy"
msgstr "HTTP பாதுகாப்பு மேம்பட்ட ப்ராக்ஸி"

#: service-list.pl.in:108
msgid ""
"Saves and restores system entropy pool for higher quality random number "
"generation."
msgstr "entropy pool அதிக தரம் வாந்த தொடர்பற்ற எண் உருவாக்கத்தை சேமிக்கும் மீட்கும்"

#: service-list.pl.in:109
msgid ""
"This scripts assignes raw devices to block devices (such as hard drive "
"partitions). This is for the use of applications such as Oracle. You can set "
"up the raw device to block device mapping by editing the file /etc/sysconfig/"
"rawdevices."
msgstr "இந்த சிறு நிரல் சாதனங்களை தடை செய்ய பயன்படும்(வன்தகடு பகிர்தல்). இதை ஆரக்கிள் போன்ற பயன்பாடுகள் பயன்படுத்துகிறது. /etc/sysconfig/ கோப்பை திருத்தி சாதனத்தை அமைக்கலாம்"

#: service-list.pl.in:110
msgid ""
"This is a daemon which handles the task of connecting periodically to the "
"Red Hat Network servers to check for updates, notifications and perform "
"system monitoring tasks according to the service level that this server is "
"subscribed for."
msgstr "இந்த மாயாவி ரெட் ஹாட் வலைப்பின்னலுடன் இணைப்பை ஏற்படுத்தி புதுப்பிக்கப்பட்ட மென்பொருள்களை நிறுவ பயன்படும். "

#: service-list.pl.in:111 service-list.pl.in:112
msgid ""
"The rsync server is a good addition to am ftp server, as it allows crc "
"checksumming etc. You must enable xinetd to use this service."
msgstr ""
"rsyc am ftp சேவகனுக்கு மாற்றாகும், இது crc "
"checksumming ஐ அனுமதிக்கும். xinetd யை செயல்படுத்தி இந்த சேவையை பெறலாம்"

#: service-list.pl.in:112
msgid "Faster, flexible replacement for rcp"
msgstr "வேகமான, எளிமையான rcp மாற்றி"

#: service-list.pl.in:113
msgid "A Windows SMB/CIFS fileserver for UNIX"
msgstr "விண்டோஸ் SMB/CIFS  UNIX கோப்பு சேவகன்"

#: service-list.pl.in:113
msgid ""
"The Samba software suite is a collection of programs that implements the "
"Server Message Block protocol for UNIX systems."
msgstr "யூனிக்ஸ் சேவகன் மேலாண்மைக்கு பயன்படும் Samba சேவகன்"

#: service-list.pl.in:114
msgid ""
"saslauthd is a server process which handles plaintext authentication "
"requests on behalf of the cyrus-sasl library."
msgstr "saslauthd cyrus-sasl நூலகத்திற்காக வெற்று உரைகளின் அனுமதியை பரிசோதிக்க பயன்படும் மென்பொருள்"

#: service-list.pl.in:115
msgid ""
"Sendmail is a Mail Transport Agent, which is the program that moves mail "
"from one machine to another."
msgstr "சென்ட்மெயில் மின்னஞ்சலை ஒரு கணினியிலிருந்து மற்றதற்கு அனுப்ப பயன்படும் சாதனம்"

#: service-list.pl.in:116
msgid ""
"An internal xinetd service, listing active servers. You must enable xinetd "
"to use this service."
msgstr "உள் xinetd சேவை செயல் சேவகனை கண்காணிக்கும். இதற்கு xinetd செயல்பாட்டில் இருக்க வேண்டும்"

#: service-list.pl.in:117
msgid ""
"An internal xinetd service, listing active services. You must enable xinetd "
"to use this service."
msgstr "உள் xinetd சேவை செயல் சேவகனை கண்காணிக்கும். இதற்கு xinetd செயல்பாட்டில் இருக்க வேண்டும்"

#: service-list.pl.in:118
msgid "get/set serial port information"
msgstr "get/set நேர் பாதை தகவல்"

#: service-list.pl.in:118
msgid ""
"setserial is a program designed to set and/or report the configuration "
"information associated with a serial port."
msgstr "setserial நிரல் நேர் பாதை அமைப்பு சார்ந்த தகவல்கலை அமைக்க மற்றும் அறிக்கை தயாரிக்க அமைக்கப்பட்டுள்ளது"

#: service-list.pl.in:119
msgid ""
"FAM is a file monitoring daemon. It can be used to get reports when files "
"change. You must enable xinetd to use this service."
msgstr "FAM கோப்பு மேலாண்மை மாயாவி. இது கோப்புகளின் செய்த மாற்றங்கள் பற்றிய அறிக்கையை தரும்.  இதற்கு xinetd செயல்பாட்டில் இருக்க வேண்டும்"

#: service-list.pl.in:120
msgid "control and monitor storage systems using S.M.A.R.T."
msgstr "g S.M.A.R.T. பயன்படுத்தி சேமிப்பு அமைப்புகளை கட்டுப்படுத்து"

#: service-list.pl.in:121
msgid "Perl-based spam filter using text analysis"
msgstr "Perl-based spam வடிகட்டி அலசல்"

#: service-list.pl.in:122
msgid "Simple Network Management Protocol (SNMP) Daemon"
msgstr "Simple Network Management Protocol (SNMP) மாயாவி"

#: service-list.pl.in:123 service-list.pl.in:124
msgid "OpenSSH SSH server"
msgstr "OpenSSH SSH சேவகன்"

#: service-list.pl.in:123
msgid ""
"ssh is a program for logging into a remote machine and for executing "
"commands on a remote machine."
msgstr "ssh தொலை கணினியில் நுழைந்து கோப்புகளை அலச பயன்படும்"

#: service-list.pl.in:124
msgid "OpenSSH server daemon"
msgstr "OpenSSH சேவகன் மாயாவி"

#: service-list.pl.in:125
msgid "Linux system logging utilities"
msgstr "லினக்ஸ் கணினி உள்நுழை பயன்பாடு"

#: service-list.pl.in:125
msgid ""
"Sysklogd provides two system utilities which provide support for system  "
"logging  and  kernel  message  trapping."
msgstr "Sysklogd உள்நுழைய மற்றும் கர்னல் மேலாண்மைக்கு பயன்படும்"

#: service-list.pl.in:126
msgid ""
"Syslog is the facility by which many daemons use to log messages to various "
"system log files.  It is a good idea to always run syslog."
msgstr "Syslog பயன்படுத்தி கணினியில் பல்வேறு செயல்களை பதிவு செய்யலாம்"

#: service-list.pl.in:127
msgid ""
"An RFC 868 time server. This protocol provides a site-independent, machine "
"readable date and time. The Time service sends back to the originating "
"source the time in seconds since midnight on January first 1900.  This is "
"the tcp version. You must enable xinetd to use this service."
msgstr "RFC 868 நேர சேவகம்.இது இணைய தளம் சார்ந்த நேரம் தேதி அமைக்கும் விதிமுறை. இது 1900 ஜனவரி என அமைக்கப்பட்டிருக்கும், இது tcp பதிப்பு. இந்த சேவையை பயன்படுத்த xinetd செயல்பாட்டில் இருக்க வேண்டும்"

#: service-list.pl.in:128
msgid ""
"An RFC 868 time server. This protocol provides a site-independent, machine "
"readable date and time. The Time service sends back to the originating "
"source the time in seconds since midnight on January first 1900.  This is "
"the udp version. You must enable xinetd to use this service."
msgstr "RFC 868 நேர சேவகம்.இது இணைய தளம் சார்ந்த நேரம் தேதி அமைக்கும் விதிமுறை. இது 1900 ஜனவரி என அமைக்கப்பட்டிருக்கும், இது ucp பதிப்பு. இந்த சேவையை பயன்படுத்த xinetd செயல்பாட்டில் இருக்க வேண்டும்"

#: service-list.pl.in:129
msgid "kernel random number source devices"
msgstr "கர்னர் தொடரற்ற எண் மூல சாதனம்"

#: service-list.pl.in:129
msgid ""
"The  random  number  generator  gathers environmental noise from device "
"drivers and other sources into an entropy pool. From this entropy pool "
"random numbers are created."
msgstr "entropy pool லிருந்து தொடரற்ற எண்களை உருவாக்கி கண்காணிக்க தொடரற்ற எண் உருவாக்கி பயன்படும்"

#: service-list.pl.in:131
msgid "Intel(TM) x86(TM)-based virtual machine"
msgstr "Intel(TM) x86(TM)-சார்ந்த மெய்நிகர் கணினி"

#: service-list.pl.in:131
msgid ""
"The vmware command starts an Intel X86-based virtual machine. This script "
"manages the services needed to run VMware software."
msgstr "vmware இன்டல் X86-சார்ந்த மெய்நிகர் கணினியை துவக்கும். சேவை சார்ந்த VMware மென்பொருளை இயக்க இது பயன்படும்."

#: service-list.pl.in:132
msgid "Web-based administration toolkit"
msgstr "இணையம் சார்ந்த மேலாண்மை கருவி"

#: service-list.pl.in:133
msgid "Name Service Switch daemon for resolving names from NT servers"
msgstr "NT சேவகன்களின் பெயரை அறிய பயன்படும் சேவை"

#: service-list.pl.in:133
msgid "Starts and stops the Samba winbind daemon"
msgstr "Samba winbind மாயாவியை துவக்கு நிறுத்து"

#: service-list.pl.in:134
msgid ""
"Allow users to run Windows(tm) applications by just clicking on them (or "
"typing ./file.exe)"
msgstr ""
"Windows(tm) பயன்பாடுகளை பயனர் பயன்படுத்த அனுமதி (or "
"typing ./file.exe)"

#: service-list.pl.in:135
msgid "X Display Manager"
msgstr "X காட்சி மேலாளர்"

#: service-list.pl.in:135
msgid "Provides the X equivalent of a \"login:\" prompt."
msgstr "X க்கு சமமான \"உள்நுழை:\" வசதி தரும்"

#: service-list.pl.in:136
msgid "X font server"
msgstr "X எழுத்துரு சேவகன்"

#: service-list.pl.in:136
msgid ""
"Xfs is the X Window System font server. It supplies fonts to X Window System "
"display servers."
msgstr "Xfs X சாளர எழுத்துரு சேவகன்.இது X சாளரத்திற்கு தேவையான எழுதுருக்களை காட்சி சேவனுக்கு அனுப்பும்."

#: service-list.pl.in:137
msgid ""
"xinetd is a powerful replacement for inetd. xinetd has access control "
"machanisms, extensive logging capabilities, the ability to make services "
"available based on time, and can place limits on the number of servers that "
"can be started, among other things."
msgstr "xinetd inetd க்கான மாற்றாகும். xinetd யில் உள்ள கட்டுப்பாடு தொழில் நுட்பம், அதிக திறம் படைத்தது, இது சேவைகளை உடனுக்குடன் தருவதோடு, துவங்க வேண்டிய சேவைகளின் எண்ணிக்கையையும் கட்டுப்பாட்டில் வைக்கும்."

#: service.pl.in:226 service.pl.in:231
msgid "Halting the system"
msgstr "கணினியை நிறுத்துகிறது"

#: service.pl.in:227 service.pl.in:233
msgid "Text mode"
msgstr "உரை பாங்கு"

#: service.pl.in:228 service.pl.in:232 service.pl.in:241
msgid "Graphical mode"
msgstr "காட்சி பாங்கு"

#: service.pl.in:229 service.pl.in:234
msgid "Rebooting the system"
msgstr "கணினியை மீண்டும் துவக்குகிறது"

#: service.pl.in:236
msgid "Starts all system neccesary services"
msgstr "எல்லா தேவையான சேவைகளையும் துவக்கு"

#: service.pl.in:237
msgid "Default runlevel"
msgstr "இயல்பான இயக்க மட்டம்"

#: service.pl.in:238
msgid "Networkless runlevel"
msgstr "வலைப்பின்னல் இல்லா இயக்க மட்டம்"

