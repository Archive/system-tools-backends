# Turkish translation of setup-tools-backends.
# Copyright (C) 2003
# This file is distributed under the same license as the PACKAGE package.
# Gorkem Cetin <gorkem@bahcesehir.edu.tr>, 2003.
# Rıdvan CAN <ridvancan@linux-sevenler.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: setup-tools-backends\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-09-09 20:59+0200\n"
"PO-Revision-Date: 2004-09-11 03:46+0300\n"
"Last-Translator: Rıdvan CAN <ridvancan@linux-sevenler.org>\n"
"Language-Team: Turkish <gnome-turk@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=1; plural=0;\n"

#. please, keep this list sorted
#. service => [long_name, description]
#: service-list.pl.in:36
msgid "ACPI event handler"
msgstr "ACPI olay denetimcisi"

#: service-list.pl.in:36
msgid ""
"Notifies user-space programs of ACPI events: executes arbitrary commands in "
"response to ACPI events such as power buttons."
msgstr ""
"ACPI olaylarını kullanıcı-alanı programlarına bildirme: güç düğmeleri gibi ACPI "
"olaylarına yanıtta isteğe bağlı komutları yürütür."

#: service-list.pl.in:37
msgid "Advanced Linux Sound Architecture"
msgstr "Gelişmiş Linux Ses Mimarisi"

#: service-list.pl.in:38 service-list.pl.in:70
msgid "web server"
msgstr "web sunucu"

#: service-list.pl.in:38 service-list.pl.in:70
msgid "Apache is a World Wide Web server.  It is used to serve HTML files and CGI."
msgstr "Apache, HTML ve CGI sayfalarını sunmak için kullanılan bir web sunucudur."

#: service-list.pl.in:39
msgid "Run missed scheduled jobs at startup"
msgstr "Planlanmış işleri başlangıçta çalıştır"

#: service-list.pl.in:39
msgid "Run cron jobs that were left out due to downtime"
msgstr "Çalışması gereken cron işlerini çalıştır"

#: service-list.pl.in:40
msgid "Automatic power management daemon"
msgstr "Otomatik güç yönetim servisi"

#: service-list.pl.in:40
msgid ""
"apmd is used for monitoring battery status and logging it via syslog(8). It "
"can also be used for shutting down the machine when the battery is low."
msgstr ""
"Pil durumunu ve syslog(8) ana kütüğünü izlemek için apmd kullanılmıştır. "
"Pil düşükken makineyi kapatmak için ayrıca kullanılmış olabilir."

#: service-list.pl.in:41
msgid "Runs a command at a given time"
msgstr "Belirlenmiş bir zamanda bir komudu çalıştırır"

#: service-list.pl.in:41
msgid ""
"Runs commands scheduled by the at command at the time specified when at was "
"run, and runs batch commands when the load average is low enough."
msgstr ""
"Belirli bir zamanda çalıştırılması istenen komutları çalıştırır, ve koşu yığının yük "
"ortalamasının ne zaman yeterli düşüklükte olduğuna hükmeder."

#: service-list.pl.in:42
msgid "Audio mixer"
msgstr "Ses karıştırıcı"

#: service-list.pl.in:42
msgid "This program adjusts the settings of an audio mixing device"
msgstr "Bu program ses karıştıcı aygıtın düzenlerini ayarlar."

#: service-list.pl.in:43
msgid "BinFormat Support"
msgstr "Bin Biçim Desteği"

#: service-list.pl.in:43
msgid "Support for running non ELF binaries on Linux"
msgstr "Linux üzerinde ELF binari çalıştırma desteği"

#: service-list.pl.in:44
msgid "Boot messages recorder"
msgstr "Önyükleme mesaj kaydedici"

#: service-list.pl.in:44
msgid ""
"Runs in the background and copies all strings sent to the /dev/console "
"device to a logfile"
msgstr ""
"Arka planda çalışan ve kopya tüm dizileri bir kütük dosyasıyla /dev/console "
"aygıtına gönderir."

#: service-list.pl.in:45
msgid ""
"An xinetd internal service which generate characters. The xinetd internal "
"service which continuously generates characters until the connection is "
"dropped.  This is the tcp version. You must enable xinetd to use this "
"service."
msgstr ""
"Karakterler üreden bir xinetd içsel servisi. İptal edilen bağlantıya kadar "
"sürekli karakterler üreden xinetd içsel servisi. Bu tcp sürümüdür. Bu servisi "
"kullanmak için xinetd etkin olmalıdır."

#: service-list.pl.in:46
msgid ""
"An xinetd internal service which generate characters. The xinetd internal "
"service which continuously generates characters until the connection is "
"dropped.  This is the udp version. You must enable xinetd to use this "
"service."
msgstr ""
"Karakterler üreden bir xinetd içsel servisi. İptal edilen bağlantıya kadar "
"sürekli karakterler üreden xinetd içsel servisi. Bu udp sürümüdür. Bu servisi "
"kullanmak için xinetd etkin olmalıdır."

#: service-list.pl.in:47
msgid "CPU frequency monitor"
msgstr "MİB frekans gözlemcisi"

#: service-list.pl.in:47
msgid ""
"cpufreqd is used to monitor the status of the battery and adjust the "
"frequency of the CPU accordingly in order to preserve battery power while "
"providing optimal performance"
msgstr ""
"pilin durumunu izlemek için cpufreqd kullanılmış ve pil gücünü korumak için "
"işlemcinin frekansını ayarlayarak en uygun düzende çalışmasını temin eder"

#: service-list.pl.in:48 service-list.pl.in:130
msgid "daemon to execute scheduled commands"
msgstr "planlanan komutları yürütmek için servis"

#: service-list.pl.in:49
msgid "Run scheduled jobs"
msgstr "Planlanmış işleri çalıştır"

#: service-list.pl.in:49
msgid ""
"cron is a standard UNIX program that runs user-specified programs at "
"periodic scheduled times. vixie cron adds a number of features to the basic "
"UNIX cron, including better security and more powerful configuration options."
msgstr ""
"cron standart bir UNIX'de planlanmış zamanlarda planlanmış görevleri kullanıcı "
"tanımlarına göre periyodik çalıştır, vixie cron temel UNIX cron'a özellikler ekler, "
"daha iyi güvenlik ve daha iyi yapılandırma seçeneklerini içerir."

#: service-list.pl.in:50 service-list.pl.in:51
msgid "Common Unix Printing System"
msgstr "Genel Unix Yazdırma Sistemi (CUPS)"

#: service-list.pl.in:50 service-list.pl.in:51
msgid "Printing system based upon the Internet Printing Protocol, version  1.1"
msgstr "İnternet Yazdırma İletişim kuralına esaslı yazdırma, sürüm   1.1"

#: service-list.pl.in:52
msgid ""
"An internal xinetd service which gets the current system time then prints it "
"out in a format like this: 'Wed Nov 13 22:30:27 EST 2002'.  This is the tcp "
"version. You must enable xinetd to use this service."
msgstr ""
"Bu biçimde yazdırma olduğu zaman geçerli sistem zamanı ile zamanlama xinetd"
" servisiyle yapılır: 'Çar Kas 13 22:30:27 EST 2002'. Bu tcp sürümüdür. Bu servisi "
"kullanmak için xinetd etkin olmalıdır."

#: service-list.pl.in:53
msgid ""
"An internal xinetd service which gets the current system time then prints it "
"out in a format like this: 'Wed Nov 13 22:30:27 EST 2002'.  This is the udp "
"version. You must enable xinetd to use this service."
msgstr ""
"Bu biçimde yazdırma olduğu zaman geçerli sistem zamanı ile zamanlama xinetd"
" servisiyle yapılır: 'Çar Kas 13 22:30:27 EST 2002'. Bu udp sürümüdür. Bu servisi "
"kullanmak için xinetd etkin olmalıdır."

#: service-list.pl.in:54 service-list.pl.in:55
msgid "Dynamic DNS Client"
msgstr "Dinamik DNS İstemci"

#: service-list.pl.in:54
msgid "A Dynamic DNS Name service from www.dyndns.org"
msgstr "www.dyndns.org dan Dinamik DNS İsim servisi"

#: service-list.pl.in:55
msgid "A Dynamic DNS Name service from www.dhis.org"
msgstr "www.dhis.org dan Dinamik DNS İsim servisi"

#: service-list.pl.in:56
msgid "Dict Dictionary Server"
msgstr "Dict Sözlük Sunucu"

#: service-list.pl.in:57
msgid ""
"An xinetd internal service which echo's characters back to clients.  This is "
"the tcp version. You must enable xinetd to use this service."
msgstr ""
"İstemciler arkasında echo karakterleri xinetd içsel servisi.  Bu "
"tcp sürümüdür. Bu servisi kullanabilmek için xinetd etkin olmalıdır."

#: service-list.pl.in:58
msgid ""
"An xinetd internal service which echo's characters back to clients.  This is "
"the udp version. You must enable xinetd to use this service."
msgstr ""
"İstemciler arkasında echo karakterleri xinetd içsel servisi.  Bu "
"udp sürümüdür. Bu servisi kullanabilmek için xinetd etkin olmalıdır."

#: service-list.pl.in:59
msgid "Sound mixing server"
msgstr "Ses karıştırma sunucusu"

#: service-list.pl.in:59
msgid "The Enlightened Sound Daemon"
msgstr "Enlightened Ses Servisi"

#: service-list.pl.in:60
msgid "Exim SMTP Server"
msgstr "Exim SMTP Sunucu"

#: service-list.pl.in:61
msgid "file alteration monitor"
msgstr "dosya değişim gözlemcisi"

#: service-list.pl.in:62
msgid "Firstboot is a druid style program that runs on the first time"
msgstr "Firstboot sihirbaz tarzı bir programdır, ilk açılışta çalıştırılır"

#: service-list.pl.in:63
msgid "Text-to-speech system"
msgstr "Konuşma-sistemine metin"

#: service-list.pl.in:63
msgid "Festival is a general purpose text-to-speech system"
msgstr "Festival genel amaçlı bir metin konuşma sistemidir"

#: service-list.pl.in:64
msgid "Mail Fetcher"
msgstr "E-posta Alıcı"

#: service-list.pl.in:64
msgid "A Mail Fetcher from external Servers"
msgstr "Harici Sunuculardan E-posta Alıcı"

#: service-list.pl.in:65
msgid "IPv6 Tunnel Broker"
msgstr "IPv6 Tünelleyici"

#: service-list.pl.in:65
msgid "A IPv6 Dynamic Tunnel Broker"
msgstr "IPv6 Tünel Temsilcisi"

#: service-list.pl.in:66
msgid "GNOME Display Manager"
msgstr "GNOME Görüntü Yöneticisi"

#: service-list.pl.in:66
msgid "gdm provides the GNOME equivalent of a \"login:\" prompt."
msgstr "gdm bir \"giriş:\" bilgi isteminin GNOME'deki karşılığıdır."

#: service-list.pl.in:67
msgid "console mouse support"
msgstr "konsol fare desteği"

#: service-list.pl.in:67
msgid ""
"GPM adds mouse support to text-based Linux applications such the Midnight "
"Commander. It also allows mouse-based console cut-and-paste operations, and "
"includes support for pop-up menus on the console."
msgstr ""
"GPM Midnight Commander gibi metin bazlı Linux uygulamlarına fare desteği "
"ekler. Ayrıca konsol tabanlı fareyle kes-ve-yapıştırma işlemlerine izin verir, ve "
"konsoldaki açılan menüler için desteği içerir."

#: service-list.pl.in:68
msgid "Set hard disk parameters"
msgstr "Sabit disk parametreleri"

#: service-list.pl.in:68
msgid ""
"Provides a interface to various hard disk ioctls supported by the stock "
"Linux ATA/IDE device driver subsystem."
msgstr ""
"Disklerin çoğunlukla desteklediği bir arayüzeyi Linux ATA/IDE aygıt "
"sürücüsü alt sistemi temin eder."

#: service-list.pl.in:69
msgid "Linux hotplugging support"
msgstr "Linux hotplugging desteği"

#: service-list.pl.in:69
msgid ""
"hotplug is a program which is used by the kernel to notify user mode "
"software when some significant (usually hardware-related) events take place. "
"An example is when a USB or Cardbus device has just been plugged in."
msgstr ""
"hotplug programı kullanıcı kipinde çekirdekle kullanılmış önemli "
"yazılımların (genellikle donanım ilişkili) yerleşimini düzenler. "
"Örneğin bir USB ya da Cardbus aygıtı takıldığında."

#: service-list.pl.in:71
msgid "internet superserver"
msgstr "İnternet süper sunucusu"

#: service-list.pl.in:71
msgid ""
"inetd allows running one daemon to invoke several others, reducing load on "
"the system."
msgstr ""
"diğer programları çağırmak için inetd'de çalışmalarına izin verir, sistem "
"yükünü azaltır."

#: service-list.pl.in:72
msgid "administration tool for IPv4 packet filtering and NAT"
msgstr "IPv4 paket filtresi ve NAT için yönetim araçları"

#: service-list.pl.in:72
msgid ""
"Iptables is used to set up, maintain, and inspect the tables of IP packet "
"filter rules in the Linux kernel."
msgstr ""
"Kurulum için iptables kullanılır, sürdürülür, ve IP paket filtresi kuralları Linux çekirdeği "
"tarafından kontrol edilir."

#: service-list.pl.in:73
msgid "IrDA stack for Linux"
msgstr "Linux için IrDA desteği"

#: service-list.pl.in:74
msgid "The Internet Key Exchange protocol"
msgstr "Internet Key Exchange Protocol"

#: service-list.pl.in:74
msgid "The Internet Key Exchange protocol openbsd implementation"
msgstr "İnternet Anahtar Değişimi iletişim kuralı openbsd çalışması"

#: service-list.pl.in:75
msgid "start and stop ISDN services"
msgstr "ISDN servislerini durdurma ve başlatma"

#: service-list.pl.in:76
msgid "Check and fix joystick device nodes"
msgstr "Oyun çubuğu aygıt düğümlerini denetle ve düzelt"

#: service-list.pl.in:76
msgid ""
"Runs joystick-device-check.  joystick-device-check is a program which "
"ensures that the /dev/js* device nodes are correct for the kernel version in "
"use."
msgstr ""
"Oyun çubuğu çalışmasını denetler.  oyun çubuğu aygıt denetimlerinde çekirdek "
"sürümü için /dev/js* program düğümü kullanılır."

#: service-list.pl.in:77
msgid ""
"This package loads the selected keyboard map as set in /etc/sysconfig/"
"keyboard.  This can be selected using the kbdconfig utility.  You should "
"leave this enabled for most machines."
msgstr ""
"Bu paket /etc/sysconfig/ klavye olarak seçilmiş klavye haritasını yükler. "
"Bu kbdconfig yararlılığını kullanmayı seçilebilir.  Birçok makina da bu seçili "
"kılınmıştır."

#: service-list.pl.in:78
msgid "Kernel Log Daemon"
msgstr "Kernel Günlük Servisi"

#: service-list.pl.in:78
msgid "klogd is a system daemon which intercepts and logs Linux kernel messages."
msgstr "klogd sistem programını ve Linux çekirdek mesajları günlüğünü kesiştirir."

#: service-list.pl.in:79
msgid "This runs the hardware probe, and optionally configures changed hardware."
msgstr "Bu çalışan donanımı sorgular, ve isteğe göre değiştirilmiş donanımı yapılandırır."

#: service-list.pl.in:80 service-list.pl.in:81
msgid ""
"LIRC is a package that allows you to decode and send infra-red signals of "
"many commonly used remote controls"
msgstr ""
"LIRC paketi uzaktan kontrollerde şifrelemeye ve kırmızı sinyal göndermekte"
"kullanılır"

#: service-list.pl.in:82
msgid "Starts misc programs that should be started"
msgstr "Başlangıçta başlatılan çeşitli programlar"

#: service-list.pl.in:83
msgid ""
"lpd is the print daemon required for lpr to work properly. It is basically a "
"server that arbitrates print jobs to printer(s)."
msgstr ""
"Genel amaçlı lpr için lpd yazdırma programı gereklidir. Sunucu temelli "
"yazıcı(lar)da yazdırma işlerini kontrol eder."

#: service-list.pl.in:84
msgid "Create devices"
msgstr "Aygıt oluştur"

#: service-list.pl.in:84
msgid ""
"Script that will create the devices in /dev used to interface with drivers "
"in the kernel"
msgstr ""
"Çekirdek sürücüleriyle arayüzey kullanmada /dev aygıtları oluşturmak "
"için betik kullanılır."

#: service-list.pl.in:85
msgid "Syslog Replacement"
msgstr "Syslog Değişimleri"

#: service-list.pl.in:85
msgid "Modern logging daemon"
msgstr "Modem günlük programı"

#: service-list.pl.in:86
msgid "Load kernel modules"
msgstr "Kernel modüllerini yükle"

#: service-list.pl.in:87
msgid "tools for managing Linux kernel modules"
msgstr "Linux kernel modül yönetimi için araçlar"

#: service-list.pl.in:88 service-list.pl.in:105
msgid "database server"
msgstr "veritabanı sunucusu"

#: service-list.pl.in:88
msgid "MySQL, a SQL-based relational database daemon"
msgstr "MySQL, SQL'ı temel alan bir programdır"

#: service-list.pl.in:89
msgid "dns server"
msgstr "dns sunucu"

#: service-list.pl.in:89
msgid ""
"named (BIND) is a Domain Name Server (DNS) that is used to resolve host "
"names to IP addresses."
msgstr ""
"named (BIND) Alan Adı Sunucusunda (DNS) kullanılmış IP adreslerini adlandırmada"
"kullanılır."

#: service-list.pl.in:90
msgid ""
"Mounts and unmounts all Network File System (NFS), SMB (Lan Manager/"
"Windows), and NCP (NetWare) mount points."
msgstr ""
"Bağlanmış ve bağlı olmayan bütün Ağ Dosya Sistemi (NFS), SMB (Ağ Yöneticisi/"
"Windows), ve NCP (NetWare) bağlantı noktaları."

#: service-list.pl.in:91
msgid ""
"Activates/Deactivates all network interfaces configured to start at boot "
"time."
msgstr ""
"Yükleme zamanı başlatılacak yapılandırılmış bütün ağ arayüzlerini "
"etkinleştir/etkisizleştir."

#: service-list.pl.in:92
msgid ""
"NFS is a popular protocol for file sharing across TCP/IP networks. This "
"service provides NFS server functionality, which is configured via the /etc/"
"exports file."
msgstr ""
"NFS TCP/IP ağlarında dosya paylaşımı için popüler iletişim kuralıdır. Bu "
"NFS servisinin işlevselliğini temin eden yapılandırma dosyaları  /etc/ altına "
"gönderilir."

#: service-list.pl.in:93
msgid ""
"NFS is a popular protocol for file sharing across TCP/IP networks. This "
"service provides NFS file locking functionality."
msgstr ""
"NFS TCP/IP ağlarında dosya paylaşımı için popüler iletişim kuralıdır. Bu "
"servis NFS dosyası kilitleme işlevselliğini temin eder."

#: service-list.pl.in:94
msgid "Name service cache daemon"
msgstr "İsim servisleri önbellek programı"

#: service-list.pl.in:94
msgid "Daemon that provides a cache for the most common name service requests."
msgstr "İsim istekleri için genel amaçlı önbellek programı."

#: service-list.pl.in:95
msgid "Network Time Protocol daemon"
msgstr "Ağ Zamanı İletişim Protokolu programı"

#: service-list.pl.in:95
msgid "ntpd is the NTPv4 daemon."
msgstr "ntpd NTPv4 programıdır."

#: service-list.pl.in:96 service-list.pl.in:97
msgid "Update the system time over the network"
msgstr "Ağ üzerinden sistem zamanını günceller."

#: service-list.pl.in:96 service-list.pl.in:97
msgid ""
"Checks the time from a Network Time Protocol (ntp) server and adjusts the "
"system time accordingly."
msgstr ""
"Ağ Zaman İletişim Kuralı (ntp) zamanı ağ üzerinden kontrol eder ve "
"sistem zamanını buna göre ayarlar."

#: service-list.pl.in:98
msgid "Network Time Protocol (NTP) server"
msgstr "Ağ Zamanı İletişim Kuralı (NTP) sunucusu"

#: service-list.pl.in:98
msgid ""
"Daemon which sets and maintains the system time-of-day in synchronism with "
"Internet standard time servers"
msgstr ""
"Program, kümeleri ve sistem zamanını internetteki standart zaman sunucuylarıyla "
"ayarlar."

#: service-list.pl.in:99
msgid "HTTP caching proxy"
msgstr "HTTP önbellekleme vekil sunucu"

#: service-list.pl.in:100
msgid "PCMCIA Services"
msgstr "PCMCIA Servisleri"

#: service-list.pl.in:101
msgid "Caching-Only DNS Server"
msgstr "Salt-önbellekleyen DNS Sunucusu"

#: service-list.pl.in:102
msgid "IPsec tunnel implementation"
msgstr "IPsec tünel yürütmesi"

#: service-list.pl.in:103
msgid "DARPA port to RPC program number mapper"
msgstr "RPC programı sayı haritalayıcı DARPA portu"

#: service-list.pl.in:103
msgid ""
"The portmapper manages RPC connections, which are used by protocols such as "
"NFS and NIS. The portmap server must be running on machines which act as "
"servers for protocols which make use of the RPC mechanism."
msgstr ""
"Portmapper RPC bağlantılarını kullanan NFC ve NIS gibi iletişimlerini yönetir. RPC "
"mekanizmasını kullanan iletişim kuralları için sunucularda portmap sunucusu çalıştırılır."

#: service-list.pl.in:104
msgid "Mail Transport Agent"
msgstr "Posta Taşıma Aracı"

#: service-list.pl.in:106
msgid "Point to Point Protocol daemon"
msgstr "Noktadan Noktaya İletişim Kuralı programı"

#: service-list.pl.in:106
msgid ""
"The  Point-to-Point Protocol (PPP) provides a method for transmitting "
"datagrams over serial point-to-point links."
msgstr ""
"Noktadan Noktaya İletişim Kuralı gönderilen veri pakedi "
"için bir yöntem temin eder noktadan noktaya bağlar"

#: service-list.pl.in:107
msgid "Privacy enhancing HTTP Proxy"
msgstr "Gizliliği arttırılmış HTTP Vekil Sunucusu"

#: service-list.pl.in:108
msgid ""
"Saves and restores system entropy pool for higher quality random number "
"generation."
msgstr "Yüksek kaliteli rasgele sayısı üretimi için sistem kayıt ve değiştirme sistemi."

#: service-list.pl.in:109
msgid ""
"This scripts assignes raw devices to block devices (such as hard drive "
"partitions). This is for the use of applications such as Oracle. You can set "
"up the raw device to block device mapping by editing the file /etc/sysconfig/"
"rawdevices."
msgstr ""
"Bu betik blok aygıtları taslak aygıtlara atar. (Sabit disk sürücüsü "
"olarak). Bu Oracle uygulamaları için kullanılır. /etc/sysconfig/ altındaki dosyayı "
"düzenleyerek taslak aygıtlar kurabilirsiniz."

#: service-list.pl.in:110
msgid ""
"This is a daemon which handles the task of connecting periodically to the "
"Red Hat Network servers to check for updates, notifications and perform "
"system monitoring tasks according to the service level that this server is "
"subscribed for."
msgstr ""
"Bu program periyodik aralıklarla Redhat Ağına bağlanarak sistemini güncel "
"tutmanızı sağlar, tanımlamalar ve genel sistem gözlemi için sunucu servis düzeyleri "
"kayıt eder. "

#: service-list.pl.in:111 service-list.pl.in:112
msgid ""
"The rsync server is a good addition to am ftp server, as it allows crc "
"checksumming etc. You must enable xinetd to use this service."
msgstr ""
"Rsync sunucusu iyi bir ftp sunucu toplamıdır, crc denetimlerine izin verir. "
"Bu servisi kullanabilmek için xinetd etkin olmalıdır."

#: service-list.pl.in:112
msgid "Faster, flexible replacement for rcp"
msgstr "Rcp için esnek yer değiştirme, hız"

#: service-list.pl.in:113
msgid "A Windows SMB/CIFS fileserver for UNIX"
msgstr "UNIX için Windows SMB/CIFS dosya sunucusu"

#: service-list.pl.in:113
msgid ""
"The Samba software suite is a collection of programs that implements the "
"Server Message Block protocol for UNIX systems."
msgstr ""
"Samba yazılım pakedi UNIX sistemi için Sunucu Mesaj Bloklama İletişim kuralı "
"program koleksiyonlarını çalıştırır."

#: service-list.pl.in:114
msgid ""
"saslauthd is a server process which handles plaintext authentication "
"requests on behalf of the cyrus-sasl library."
msgstr ""
"saslauthd, cyrus-sasl kütüphanesinin behalf isteklerini plaintext doğrulamasıyla "
"ele alan sunucu işlemidir."

#: service-list.pl.in:115
msgid ""
"Sendmail is a Mail Transport Agent, which is the program that moves mail "
"from one machine to another."
msgstr ""
"Sendmail bir Posta Taşıma Aracısıdır, postaları bir makinadan diğer bir makinaya "
"taşıyan programdır."

#: service-list.pl.in:116
msgid ""
"An internal xinetd service, listing active servers. You must enable xinetd "
"to use this service."
msgstr ""
"Bir içsel xinetd servisi, aktif sunucuları listeler. Bu servisi kullanmak için "
"xinetd etkin olmalıdır."

#: service-list.pl.in:117
msgid ""
"An internal xinetd service, listing active services. You must enable xinetd "
"to use this service."
msgstr ""
"Bir içsel xinetd servisi, aktif servisleri listeler. Bu servisi kullanmak için "
"xinetd etkin olmalıdır."

#: service-list.pl.in:118
msgid "get/set serial port information"
msgstr "alma/küme seri post bilgisi"

#: service-list.pl.in:118
msgid ""
"setserial is a program designed to set and/or report the configuration "
"information associated with a serial port."
msgstr ""
"setserial küme programlar için tasarlanmıştır ve/ya da  seri portla "
"ilişkilendirilmiş yapılandırmalar için rapor verir."

#: service-list.pl.in:119
msgid ""
"FAM is a file monitoring daemon. It can be used to get reports when files "
"change. You must enable xinetd to use this service."
msgstr ""
"FAM dosya gözleme programıdır. Dosya değişimlerini olduğu zaman rapor "
"almada kullanılır. Bu servisin çalışması için xinetd etkin olmalıdır."

#: service-list.pl.in:120
msgid "control and monitor storage systems using S.M.A.R.T."
msgstr "denetim ve sistem bellek gözleminde S.M.A.R.T kullanılır."

#: service-list.pl.in:121
msgid "Perl-based spam filter using text analysis"
msgstr "Perl tabanlı spam filtresi kullanma metin analizi"

#: service-list.pl.in:122
msgid "Simple Network Management Protocol (SNMP) Daemon"
msgstr "Simple Network Management Protocol (SNMP) Programı"

#: service-list.pl.in:123 service-list.pl.in:124
msgid "OpenSSH SSH server"
msgstr "OpenSSH SSH sunucusu"

#: service-list.pl.in:123
msgid ""
"ssh is a program for logging into a remote machine and for executing "
"commands on a remote machine."
msgstr ""
"ssh programı uzak makina komutlar çalıştırmak için ve uzak makinaya girişleri "
"sağlar."

#: service-list.pl.in:124
msgid "OpenSSH server daemon"
msgstr "OpenSSH sunucu programı"

#: service-list.pl.in:125
msgid "Linux system logging utilities"
msgstr "Linux sistem günlüğü yardımcıları"

#: service-list.pl.in:125
msgid ""
"Sysklogd provides two system utilities which provide support for system  "
"logging  and  kernel  message  trapping."
msgstr "Sysklogd iki şeyi temin eder, çekirdek mesajlarını ve sistem günlüklerini"

#: service-list.pl.in:126
msgid ""
"Syslog is the facility by which many daemons use to log messages to various "
"system log files.  It is a good idea to always run syslog."
msgstr ""
"Syslog kolaylık açısından birçok sistem programını ve sistem günlük dosyalarını kullanır. "
"Bu yüzden her zaman çalıştırılmalıdır."

#: service-list.pl.in:127
msgid ""
"An RFC 868 time server. This protocol provides a site-independent, machine "
"readable date and time. The Time service sends back to the originating "
"source the time in seconds since midnight on January first 1900.  This is "
"the tcp version. You must enable xinetd to use this service."
msgstr ""
"RFC 868 zaman sunucusudur. Bu iletişim kuralı makina tarafından okunabilen tarih ve "
"zaman yapar. İlk 1900 Ocak gecesinden başlayarak yıllı, saniyeyi zamanı istendiğinde "
"zaman servisine gönderir. Bu tcp sürümüdür. Bu servisi kullanabilmek için xinetd etkin "
"olmalıdır."

#: service-list.pl.in:128
msgid ""
"An RFC 868 time server. This protocol provides a site-independent, machine "
"readable date and time. The Time service sends back to the originating "
"source the time in seconds since midnight on January first 1900.  This is "
"the udp version. You must enable xinetd to use this service."
msgstr ""
"RFC 868 zaman sunucusudur. Bu iletişim kuralı makina tarafından okunabilen tarih ve "
"zaman yapar. İlk 1900 Ocak gecesinden başlayarak yıllı, saniyeyi zamanı istendiğinde "
"zaman servisine gönderir. Bu udp sürümüdür. Bu servisi kullanabilmek için xinetd etkin "
"olmalıdır."

#: service-list.pl.in:129
msgid "kernel random number source devices"
msgstr "çekirdek rasgele sayı kaynağı aygıtları"

#: service-list.pl.in:129
msgid ""
"The  random  number  generator  gathers environmental noise from device "
"drivers and other sources into an entropy pool. From this entropy pool "
"random numbers are created."
msgstr ""
"Rasgele sayı üretici aygıt sürücülerinden haber yayan çevreleri ve diğer "
"kaynakları bir entropi havuzuna toplar. Bu entropi havuzundan "
"rasgele sayılar oluşturulur."

#: service-list.pl.in:131
msgid "Intel(TM) x86(TM)-based virtual machine"
msgstr "Intel(TM) x86(TM) tabanlı sanal makina"

#: service-list.pl.in:131
msgid ""
"The vmware command starts an Intel X86-based virtual machine. This script "
"manages the services needed to run VMware software."
msgstr ""
"Vmware komutla başlatılan İntel X86 tabanlı sanal makinadır. Bu betiğin yönetimi "
"VMware yazılımı servisleri ile yapılır."

#: service-list.pl.in:132
msgid "Web-based administration toolkit"
msgstr "Web tabanlı yönetim araçları"

#: service-list.pl.in:133
msgid "Name Service Switch daemon for resolving names from NT servers"
msgstr "NT sunuculardan isimleri dönüştürmek için İsim Servisi Değiştirici program"

#: service-list.pl.in:133
msgid "Starts and stops the Samba winbind daemon"
msgstr "Samba winbind programını durdurur ve başlatır"

#: service-list.pl.in:134
msgid ""
"Allow users to run Windows(tm) applications by just clicking on them (or "
"typing ./file.exe)"
msgstr ""
"Kullanıcıların tıklayıp Windows(tm) uygulamalarını çalıştırmalarına "
"izin verir. (ya da ./file.exe türünde)"

#: service-list.pl.in:135
msgid "X Display Manager"
msgstr "X Görüntü Yöneticisi"

#: service-list.pl.in:135
msgid "Provides the X equivalent of a \"login:\" prompt."
msgstr "Bir \" giriş \" bilgi isteminin X karşılığıdır."

#: service-list.pl.in:136
msgid "X font server"
msgstr "X yazıtipi sunucusu"

#: service-list.pl.in:136
msgid ""
"Xfs is the X Window System font server. It supplies fonts to X Window System "
"display servers."
msgstr ""
"Xfs X Window Sistemi için yazı tipi sunucusudur. X Window sistemi görüntü "
"sunucularına yazı tiplerini sağlar."

#: service-list.pl.in:137
msgid ""
"xinetd is a powerful replacement for inetd. xinetd has access control "
"machanisms, extensive logging capabilities, the ability to make services "
"available based on time, and can place limits on the number of servers that "
"can be started, among other things."
msgstr ""
"xinetd inetd için güçlü bir değiştirmedir. xinetd erişim kontrolu mekanizmasına "
"sahiptir, gelişmiş günlükleme yetenekleri, zaman tabanlı yetenekli yaptırım servisleri "
"mevcuttur, ve başlatılmış sunucu sayısı yerle sınırlıdır, diğer şeylerde içinde."

#: service.pl.in:226 service.pl.in:231
msgid "Halting the system"
msgstr "Sistem durduruluyor"

#: service.pl.in:227 service.pl.in:233
msgid "Text mode"
msgstr "Metin giriş"

#: service.pl.in:228 service.pl.in:232 service.pl.in:241
msgid "Graphical mode"
msgstr "Grafiksel giriş"

#: service.pl.in:229 service.pl.in:234
msgid "Rebooting the system"
msgstr "Sistem yeniden başlatılıyor"

#: service.pl.in:236
msgid "Starts all system neccesary services"
msgstr "Gerekli bütün sistem servislerini başlat"

#: service.pl.in:237
msgid "Default runlevel"
msgstr "Öntanımlı seviye"

#: service.pl.in:238
msgid "Networkless runlevel"
msgstr "Ağ çalışma seviyesi"

